export const myBusinessCase = {
    experiment_id: '671790bd-797a-4dc9-bb08-8326d8669c67',
    id: 'a560c71f-7c4b-45ed-9e0d-e8fc026a9286',
    name: 'new Test',
    parameters: {
      forecast: {
        enabled: true,
        model: { enabledOptions: ['latest', 'new'], refreshDataset: null },
        predictionScope: { enabledOptions: ['latest'], predictionDates: false, refreshDataset: true },
      },
      icon: 'bx-cube-alt',
      optimization: {
        datasetName: null,
        datasetSelection: 'ask',
        enabled: true,
        optimizationConstraints: true,
        optimizationDates: false,
        optimizationObjectives: true,
        variableTypes: 'float',
      },
    },
  };

export const mySynapp = {
    avatar: null,
    created_at: '2022-01-18 10:50:37.079124',
    experiments: [
      {
        configurations: ['2022-04-07 13:26:10.163330'],
        created_at: '2022-04-05 15:19:05.614809',
        id: 'efb236f3-abc4-493e-b945-eab5172a95cf',
        name: 'test-quentin',
        parameters: { cloned: '671790bd-797a-4dc9-bb08-8326d8669c67', 'cloned-full': false },
        status: 'ready',
        synapp_id: '51ae4c95-afa0-4a7e-9a09-7fc13a3683a7',
      },
      {
        configurations: ['2022-01-18 10:50:37.284163', '2022-04-07 13:03:08.820529'],
        created_at: '2022-04-07 13:01:41.492785',
        id: '7bd12332-e10c-47b3-bd18-f8449cdab5d1',
        name: 'defaultcloned',
        parameters: { cloned: '671790bd-797a-4dc9-bb08-8326d8669c67', 'cloned-full': true },
        status: 'ready',
        synapp_id: '51ae4c95-afa0-4a7e-9a09-7fc13a3683a7',
      },
    ],
    id: '51ae4c95-afa0-4a7e-9a09-7fc13a3683a7',
    name: 'veepee',
    parameters: { tag: 'latest' },
    status: 'created',
  };
export const myExperiment = {
    configurations: ['2022-01-18 10:50:37.284163'],
    created_at: '2022-01-18 10:50:37.284203',
    id: '671790bd-797a-4dc9-bb08-8326d8669c67',
    name: 'default',
    parameters: {},
    status: 'ready',
    synapp_id: '51ae4c95-afa0-4a7e-9a09-7fc13a3683a7',
  };