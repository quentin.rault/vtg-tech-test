const getAll = async (synappID) => {
    return 'ok';
};

const getOne = async (synappID, businessCaseId) => {
  return 'ok';
};

const create = async (synappID, payload) => {
  return 'ok';
};

const edit = async (synappID, businessCaseId, payload) => {
  return 'ok';
};

const deleteOne = async (synappID, businessCaseId) => {
  return 'ok';
};

const businessCasesApi = {
  getAll,
  getOne,
  create,
  edit,
  deleteOne,
};

export default businessCasesApi;
